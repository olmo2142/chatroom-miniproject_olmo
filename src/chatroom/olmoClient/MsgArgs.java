package chatroom.olmoClient;

public enum MsgArgs {
    CreateLogin,
    Login,
    ChangePassword,
    DeleteLogin,
    Logout,
    CreateChatroom,
    JoinChatroom,
    LeaveChatroom,
    DeleteChatroom,
    ListChatrooms,
    Ping,
    SendMessage,
    UserOnline,
    ListChatroomUsers,
    
    

}
