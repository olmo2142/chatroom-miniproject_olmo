package chatroom.olmoClient.Views;

import chatroom.olmoClient.Translator;
import chatroom.olmoClient.ServiceLocator;
import chatroom.olmoClient.Model.Start_Model;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Start_View extends Basic_View<Start_Model> {

    private Label lblConnection;
    private Label lblIpAddress;
    public TextField txtIpAddress;  
    public Label lblIpOK;
    private Label lblPort;
    public TextField txtPort;
    public Label lblPortOK;
    public Button btnConnect;
    private Label lblConnect;
    private Label lblSec;
    public RadioButton secYes;
    private RadioButton secNo;
    private ToggleGroup secChoice;
    public ProgressBar pi;
    public Label lblError;
    public Button btnGoLogin;
    private Menu_Start_View startMenu;
    public ControlBar_Start_View controls;
    

    public Start_View(Stage stage, Start_Model model) {
	super(stage, model);
	sl.getLogger().info("Start_View initialized");
	

	lblIpAddress.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
	lblPort.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
	btnConnect.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

	// Set sizes for top TextFields
	txtIpAddress.setMinWidth(150);
	txtIpAddress.setPrefWidth(150);
	txtPort.setMinWidth(60);
	txtPort.setPrefWidth(60);


    }

    @Override
    protected Scene create_GUI() {
	t = ServiceLocator.getServiceLocator().getTranslator();
	sl = ServiceLocator.getServiceLocator();

	// Center Start-View
	lblConnection = new Label(t.getString("start.lblConnection"));
	lblIpAddress = new Label(t.getString("start.lblIpAddress"));
	txtIpAddress = new TextField("147.86.8.31");
	lblIpOK = new Label();
	lblPort = new Label(t.getString("start.lblPort"));
	txtPort = new TextField("50001");
	lblPortOK = new Label();
	lblSec = new Label(t.getString("start.lblSec"));
	secChoice = new ToggleGroup();
	secYes = new RadioButton(t.getString("program.btnYes"));
	secYes.setToggleGroup(secChoice);
	secNo = new RadioButton(t.getString("program.btnNo"));
	secNo.setToggleGroup(secChoice);
	secNo.setSelected(true);
	btnGoLogin = new Button(t.getString("start.btnGoLogin"));
	btnGoLogin.setDisable(true);
	
	btnConnect = new Button(t.getString("start.btnConnect"));
	lblConnect = new Label(t.getString("start.lblConnect"));
	pi = new ProgressBar(0);
	pi.progressProperty();
	lblError = new Label();

	// CenterBox1
	GridPane cBox1 = new GridPane();
	cBox1.setAlignment(Pos.CENTER);
	cBox1.getStyleClass().add("grid1");
	cBox1.setId("grid1");
	cBox1.add(lblConnection, 1, 1, 2, 1);
	cBox1.add(lblIpAddress, 1, 2);
	cBox1.add(txtIpAddress, 2, 2, 2, 1);
	cBox1.add(lblIpOK, 5, 2 );
	cBox1.add(lblPort, 1, 3, 1, 1);
	cBox1.add(txtPort, 2, 3, 1, 1);
	cBox1.add(lblPortOK, 5, 3);
	cBox1.add(lblSec, 1, 4);
	cBox1.add(secYes, 2, 4);
	cBox1.add(secNo, 3, 4);
	cBox1.add(lblConnect, 2, 5, 2, 1);
	cBox1.add(pi, 2, 6, 2, 1);
	cBox1.add(lblError, 2, 7);
	cBox1.add(btnConnect, 2, 8, 2, 1);
	cBox1.add(btnGoLogin, 2, 9, 2, 1);

	// Menu
	startMenu = new Menu_Start_View();

	// ControlBar
	controls = new ControlBar_Start_View();
	controls.getStyleClass().add("controls");
	controls.setId("controls");

	// BorderPane
	BorderPane root = new BorderPane();
	root.getStyleClass().add("root");
	root.setTop(startMenu);
	root.setCenter(cBox1);
	root.setBottom(controls);

	Scene scene = new Scene(root);
	stage.setScene(scene);

	stage.setMinWidth(500);
	stage.setMinHeight(stage.getHeight());

	// scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

	stage.setTitle(t.getString("program.name"));

	return scene;
    }

   
}
