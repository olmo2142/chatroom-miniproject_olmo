package chatroom.olmoClient.Views;

import javafx.scene.control.Button;

public class ControlBar_Login_View extends ControlBar_Basic_View{
    
    public Button btnBack;
    public Button btnCancel;
    
    
    public ControlBar_Login_View() {
	super();
	btnBack = new Button(t.getString("login.btnBack"));
	btnCancel = new Button(t.getString("start.btnCancel"));
	
	btnBack.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
	
	toolLeft.getItems().addAll(btnCancel, btnBack);
	
	
    }

}
