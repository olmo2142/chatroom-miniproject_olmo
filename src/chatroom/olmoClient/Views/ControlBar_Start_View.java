package chatroom.olmoClient.Views;

import javafx.scene.control.Button;

public class ControlBar_Start_View extends ControlBar_Basic_View{
    
    public Button btnCancel;
    
    
    public ControlBar_Start_View() {
	super();
	btnCancel = new Button(t.getString("start.btnCancel"));
	
	btnCancel.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
	
	toolLeft.getItems().addAll(btnCancel);
	
	
    }

}
