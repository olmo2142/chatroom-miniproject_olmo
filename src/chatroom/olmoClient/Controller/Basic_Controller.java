package chatroom.olmoClient.Controller;

import chatroom.olmoClient.ServiceLocator;
import chatroom.olmoClient.Translator;
import chatroom.olmoClient.Model.Basic_Model;
import chatroom.olmoClient.Views.Basic_View;

public abstract class Basic_Controller<M extends Basic_Model, V extends Basic_View<M>> {
    protected M model;
    protected V view;
    protected ServiceLocator sl;
    protected Translator t;
    
    protected Basic_Controller(M model, V view) {
	this.model = model;
	this.view = view;
	sl = ServiceLocator.getServiceLocator();
	t = ServiceLocator.getServiceLocator().getTranslator();
    }

	
      

}
