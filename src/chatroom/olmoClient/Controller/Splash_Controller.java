package chatroom.olmoClient.Controller;


import chatroom.olmoClient.Chatroom_olmo;
import chatroom.olmoClient.Model.Splash_Model;
import chatroom.olmoClient.Views.Splash_View;
import javafx.concurrent.Worker;


public class Splash_Controller extends Basic_Controller<Splash_Model, Splash_View>{

    public Splash_Controller(final Chatroom_olmo main, Splash_Model model, Splash_View view) {
        super(model, view);
        
        view.progress.progressProperty().bind(model.initializer.progressProperty());

        model.initializer.stateProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue == Worker.State.SUCCEEDED)
                        main.startApp();
                });
    }
}
