package chatroom.olmoClient.Controller;

import chatroom.olmoClient.ServiceLocator;
import chatroom.olmoClient.Model.Chatroom_Model;
import chatroom.olmoClient.Model.Login_Model;
import chatroom.olmoClient.Views.Chatroom_View;
import chatroom.olmoClient.Views.Login_View;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class Login_Controller extends Basic_Controller<Login_Model, Login_View> {
    ServiceLocator serviceLocator;
    public Login_Controller(Login_Model model, Login_View view) {
	super(model, view);
	
	view.controls.btnQuit.setOnAction(e -> Platform.exit());
	view.controls.btnCancel.setOnAction(e -> clearForms());
	view.controls.btnBack.setOnAction(e -> goBack());
	view.btnSignIn.setOnAction(this::signIn);
	view.btnNewUser.setOnAction(e -> newUser());
	
    }
    
    private void signIn(Event e) {
	sl.getConfiguration().setName(view.txtName.getText());
	sl.getConfiguration().setPass(view.txtPassword.getText());	
	if (model.sendLogin()) {
	    Stage chatroomStage = new Stage();
	    Chatroom_Model chatroomModel = new Chatroom_Model();
	    Chatroom_View chatroomView = new Chatroom_View(chatroomStage, chatroomModel);
	    new Chatroom_Controller(chatroomModel, chatroomView); 
	    view.stop();
	    chatroomView.start();
	}else{
	    Alert alert = new Alert(AlertType.WARNING, t.getString("login.alertPasswordWrong"));
	    alert.showAndWait();
	    view.txtName.clear();
	    view.txtPassword.clear();
    } 
    }
	
    private void newUser() {
	if (model.newUser(view.txtName.getText(), view.txtPassword.getText())) {
	    Alert alert = new Alert(AlertType.INFORMATION, t.getString("Login.LoginCreated"));
		alert.show();
    }else {
	Alert alert = new Alert(AlertType.WARNING, t.getString("Login.LoginFail"));
	alert.show();
	view.txtName.clear();
	view.txtPassword.clear();
    }
    }
//		try {
//			
//			if(username.length() <3 || password.length() <3) {
//				//view.status.setText("Password or Username to short (min 3 letters)");	
//			}else {
//				//Pruefen ob das Login auf dem Server existiert
//				
//					String senden = "Login|"+username+"|"+password;
//					
//					model.send(senden);
//					
//					sl.getConfiguration().setUser(username);
//
//	               
//	              
//					
//				if(sl.getConfiguration().getLogin()==true) {
//					
//					sl.getLogger().info("Login succsesfull");
//				
//					//template.startApp();
//					
//					view.stage.close();
//					//Hier wird AccountCreated auf true gesetzt nun kann in Sort Message (Configurator) keine Nachricht mehr f�r den createaccount empfangen werden
//					//servicelocator.getConfiguration().setAccountCreated(true);
//					
//				}else {
//					
//					//view.status.setText("wrong password or Username");
//				}
//			}
//			}catch (Exception ex) {
//				
//				//view.status.setText("Account don't exist");
//			}
//			view.txtName.clear();
//			view.txtPassword.clear();
//		
//		
//		
//		
//		
//		
//		
//	Stage chatroomStage = new Stage();
//        Chatroom_Model chatroomModel = new Chatroom_Model();
//        Chatroom_View chatroomView = new Chatroom_View(chatroomStage, chatroomModel);
//        new Chatroom_Controller(chatroomModel, chatroomView); 
//        view.stop();
//        chatroomView.start();
//    }
//    
//
//    
//    
//    
//    
//    
    
    private void goBack() {
	view.stop();
	//Start_Controller.showStart();
	
    }
    private void clearForms() {
	view.txtName.setText("");
	view.txtPassword.setText("");
	view.txtPassword2.setText("");
	
	
    }

}
