package chatroom.olmoClient.Controller;

import chatroom.olmoClient.Chatroom_olmo;
import chatroom.olmoClient.ServiceLocator;
import chatroom.olmoClient.Model.Chatroom_Model;
import chatroom.olmoClient.Model.Login_Model;
import chatroom.olmoClient.Model.Start_Model;
import chatroom.olmoClient.Views.Chatroom_View;
import chatroom.olmoClient.Views.Login_View;
import chatroom.olmoClient.Views.Start_View;
import chatroom.server.Chatroom;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Start_Controller extends Basic_Controller<Start_Model, Start_View> {

    public Start_Controller(Start_Model model, Start_View view) {
	super(model, view);
	sl.getLogger().info("Start_Controller initialized");

	view.controls.btnQuit.setOnAction(e -> Platform.exit());
	view.controls.btnCancel.setOnAction(e -> clearForms());
	view.btnConnect.setOnAction(e -> btnConnect());
	view.btnGoLogin.setOnAction(e -> goLogIN());

	view.txtIpAddress.focusedProperty().addListener(new InvalidationListener() {
	    @Override
	    public void invalidated(Observable observable) {
		if (view.txtIpAddress.isFocused()) {
		} else {
		    checkIP();
		}
	    }
	});

	view.txtPort.focusedProperty().addListener(new InvalidationListener() {
	    @Override
	    public void invalidated(Observable observable) {
		if (view.txtPort.isFocused()) {
		} else {
		    checkPort();
		}
	    }
	});

	model.connector.stateProperty().addListener((observable, oldV, newVal) -> {
	    if (newVal == Worker.State.SUCCEEDED) {
		view.btnGoLogin.setDisable(false);
	    	view.controls.btnCancel.setDisable(false);
	    	if(model.error.get() == true){
		view.lblError.setText(t.getString("start.lblError"));
	    	}
	    }
	});
	
	//TODO noch entfernen
	model.error.addListener((observable, oldV, newVal) -> {
	    if (newVal) {
		//Chatroom_olmo.changeLocales();
		view.lblError.setText(t.getString("start.lblError"));
		System.out.println("in Listener gelandet");
	    }
	});

	view.getStage().setOnCloseRequest(new EventHandler<WindowEvent>() {
	    @Override
	    public void handle(WindowEvent event) {
		Platform.exit();
	    }
	});
    }

    private Object goLogIN() {
	Stage loginStage = new Stage();
        Login_Model loginModel = new Login_Model();
        Login_View loginView = new Login_View(loginStage, loginModel);
        new Login_Controller(loginModel, loginView); 
        view.stop();
        loginView.start();
	return null;
    }

    public void checkIP() {
   	String ip = view.txtIpAddress.getText();
   	if (Start_Model.validateIpAddress(ip)) {
   	    sl.getConfiguration().setIp(ip);
   	    view.lblIpOK.setText(t.getString("program.ok"));
   	} else {
   	    view.lblIpOK.setText(t.getString("program.nok"));
   	}
       }
    
    public void checkPort() {
	String port = view.txtPort.getText();
	if (Start_Model.validatePortNumber(port)) {
	    sl.getConfiguration().setPort(Integer.parseInt(port));
	    view.lblPortOK.setText(t.getString("program.ok"));
	} else {
	    view.lblPortOK.setText(t.getString("program.nok"));
	}
    }

   

    private Object clearForms() {
	model.disconnect();
	Chatroom_olmo.changeLocales();
	return null;
    }

    private void btnConnect() {
	view.btnConnect.setDisable(true);
	view.controls.btnCancel.setDisable(true);
	checkIP();
	checkPort();
	view.pi.progressProperty().bind(model.connector.progressProperty());
	model.connect();
    }

    public void showStart() {
	view.start();
    }

}
