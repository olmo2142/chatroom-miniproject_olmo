package chatroom.olmoClient.Model;

import java.util.logging.Logger;

import chatroom.olmoClient.MsgArgs;
import chatroom.olmoClient.ServiceLocator;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Login_Model extends Basic_Model {

    private Logger logger = Logger.getLogger("");

    public SimpleStringProperty user = new SimpleStringProperty();
    public SimpleStringProperty pass = new SimpleStringProperty();
    public SimpleBooleanProperty loginOK = new SimpleBooleanProperty();

    public Login_Model() {
	super();
	user = new SimpleStringProperty();
	pass = new SimpleStringProperty();
    }

    public boolean sendLogin() {
	boolean login = false;
	try {
	    String msgOut = Message.msgString(MsgArgs.Login, sl.getConfiguration().getName(),
		    sl.getConfiguration().getPass());
	    sl.getConfiguration().sendMsg(msgOut);
	    String receive = sl.getConfiguration().receiveMsg();
	    Message msgIn = new Message(receive);
	    if (msgIn.isMsgOk())
		System.out.println(msgIn.isMsgOk());
		login = true;
	    	logger.info("Login: " + receive);
	    	
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return login;
    }
    
    public boolean newUser(String name, String password) {
	boolean create = false;
	try {
	    if(name.length() <3 || password.length() <3) {
	    String msgOut = Message.msgString(MsgArgs.CreateChatroom, name, password);
	    sl.getConfiguration().sendMsg(msgOut);
	    String receive = sl.getConfiguration().receiveMsg();
	    Message msgIn = new Message(receive);
	    if (msgIn.isMsgOk())
		create = true;
	    logger.info("CreateNewUserAccount: " + receive);
	    }else {	
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return create;
    }

}
