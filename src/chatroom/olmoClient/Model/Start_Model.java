package chatroom.olmoClient.Model;

import java.util.logging.Logger;

import javax.net.ssl.SSLSocketFactory;

import chatroom.olmoClient.MsgArgs;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;

public class Start_Model extends Basic_Model {

    private Logger logger = Logger.getLogger("");

    private static boolean ipOk;
    private static boolean portOk;
    public boolean secure = false;
    private boolean pingOk = false;
    public BooleanProperty error = new SimpleBooleanProperty(false);

    public static boolean validateIpAddress(String ipAddress) {
	ipOk = false;
	String ipPieces[] = ipAddress.split("\\.");

	if (ipPieces.length == 4) {
	    ipOk = true;
	    int byteValue = -1;
	    for (String s : ipPieces) {
		byteValue = Integer.parseInt(s); // NumberFormatException
		if (byteValue < 0 | byteValue > 255)
		    ipOk = false;
	    }
	}
	return ipOk;
    }

    public static boolean validatePortNumber(String portText) {
	portOk = false;
	try {
	    int portNumber = Integer.parseInt(portText);
	    if (portNumber >= 1024 & portNumber <= 65535) {
		portOk = true;
	    }
	} catch (NumberFormatException e) {
	}
	return portOk;
    }

    public final Task<Void> connector = new Task<Void>() {
	@Override
	protected Void call() throws Exception {
	    this.updateProgress(-1, 5);
	    connectServer();
	    Thread.sleep(200);
	    this.updateProgress(1, 5);
	    setWriter();
	    Thread.sleep(100);
	    this.updateProgress(2, 5);
	    setReader();
	    Thread.sleep(100);
	    this.updateProgress(3, 5);
	    Thread.sleep(100);
	    this.updateProgress(4, 5);
	    sendPing();
	    this.updateProgress(4, 5);
	    checkConnection();
	    this.updateProgress(5, 5);
	    return null;
	}
    };

    private void checkConnection() throws Exception {
	try {
	    if (this.pingOk) {
		System.out.println(this.pingOk);
	    } else {
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void connect() {
	new Thread(connector).start();
    }

    protected SimpleStringProperty newestMessage = new SimpleStringProperty();

    public void connectServer() throws Exception {
	// Socket socket = null;
	// try {
	if (secure) {
	    // Registering the JSSE provider
	    // Security.addProvider(new Provider());

	    // Specifying the Truststore details. This is needed if you have created a
	    // truststore, for example, for self-signed certificates
	    System.setProperty("javax.net.ssl.trustStore", "truststore.ts");
	    System.setProperty("javax.net.ssl.trustStorePassword", "trustme");

	    // Creating Client Sockets
	    SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
	    // socket = sslsocketfactory.createSocket(sl.getConfiguration().getIp(),
	    // sl.getConfiguration().getPort());

	    // The next line is entirely optional !!
	    // The SSL handshake would happen automatically, the first time we send data.
	    // Or we can immediately force the handshaking with this method:
	    // ((SSLSocket) socket).startHandshake();
	} else {
	    sl.getConfiguration().generateSocket();
	}
    }

    public void disconnect() {
	logger.info("Disconnect");
	if (sl.getConfiguration().getSocket() != null)
	    try {
		sl.getConfiguration().closeSocket();
	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

    }

    public void sendPing() {
	try {
	    sl.getConfiguration().sendMsg(Message.msgString(MsgArgs.Ping));
	    String receive = sl.getConfiguration().receiveMsg();
	    Message msgIn = new Message(receive);
	    logger.info("Ping: " + receive);
	    if (msgIn.isMsgOk()) {
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void setWriter() throws Exception {
	sl.getConfiguration().createBufferedWriter();
    }

    public void setReader() throws Exception {
	sl.getConfiguration().createBufferedReader();
    }
}
