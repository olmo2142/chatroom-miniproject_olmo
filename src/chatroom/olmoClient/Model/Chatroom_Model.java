package chatroom.olmoClient.Model;

import java.util.ArrayList;

import chatroom.olmoClient.MsgArgs;
import chatroom.olmoClient.MsgType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Chatroom_Model extends Basic_Model {

    static String mainRoom = "--";
    static String user;
    static boolean stopReader;

    private static ObservableList<String> chatrooms;
    private static ObservableList<String> chatter;
    static ArrayList<Message> messages;
    private ObservableList<String> actualMessages;

    public Chatroom_Model() {
	super();
	Chatroom_Model.chatrooms = FXCollections.observableArrayList();
	this.chatter = FXCollections.observableArrayList();
	Chatroom_Model.messages = new ArrayList<Message>();
	this.actualMessages = FXCollections.observableArrayList();
	startStop();
    }

    public void listChatrooms() throws Exception {
	String msgOut = Message.msgString(MsgArgs.ListChatrooms, sl.getConfiguration().getToken());
	sl.getConfiguration().sendMsg(msgOut);
	String receive = sl.getConfiguration().receiveMsg();
	Message msgIn = new Message(receive);
	chatrooms.clear();
	chatrooms.addAll(msgIn.getList());
    }

    public void listChatters() throws Exception {
	if (!(mainRoom == "--")) {
	    stopReader = true;
	    String msgOut = Message.msgString(MsgArgs.ListChatroomUsers, sl.getConfiguration().getToken(), mainRoom);
	    sl.getConfiguration().sendMsg(msgOut);
	    String receive = sl.getConfiguration().receiveMsg();
	    Message msgIn = new Message(receive);
	    chatter.clear();
	    chatter.addAll(msgIn.getList());
	    stopReader = false;
	}
    }

    // eingehende Nachrichten auf ausgehende sortieren
    public ObservableList<String> listMessages() {
	stopReader = true;
	String receive;
	try {
	    receive = sl.getConfiguration().receiveMsg();
	    Message msgIn = new Message(receive);
	    if (msgIn.getMsgType().equals(MsgType.MessageText.toString())) {
		messages.add(msgIn);
		this.actualMessages.add(msgIn.getName() + ": " + msgIn.getText());
	    } else {
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	startStop();
	return actualMessages;

    }

    // Nachricht senden
    public void sendMessage(String text) throws Exception {
	stopReader = true;
	if (!(mainRoom == "--")) {
	    String msgOut = Message.msgString(MsgArgs.SendMessage, sl.getConfiguration().getToken(), mainRoom, text);
	    sl.getConfiguration().sendMsg(msgOut);
	    try {
		String receive1 = sl.getConfiguration().receiveMsg();
		Message msgIn1 = new Message(receive1);
		if (msgIn1.getMsgType().equals(MsgType.MessageText.toString())) {
		    String receive2 = sl.getConfiguration().receiveMsg();
		    msgIn1.setRead(true);
		    this.actualMessages.add(msgIn1.getName() + ": " + msgIn1.getText());
		    Message msgIn2 = new Message(receive2);
		    messages.add(msgIn1);
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
	startStop();
    }

    // Raum beitreten
    public void enterRoom(String room) throws Exception {
	stopReader = true;
	if (mainRoom == "--") {
	    String msgOut = Message.msgString(MsgArgs.JoinChatroom, sl.getConfiguration().getToken(), room,
		    sl.getConfiguration().getName());
	    sl.getConfiguration().sendMsg(msgOut);
	    String receive = sl.getConfiguration().receiveMsg();
	    Message msgIn = new Message(receive);
	    if (msgIn.isMsgOk()) {
		mainRoom = room;
	    } else {
		Alert alert = new Alert(AlertType.WARNING, t.getString("chatroom.alertEnterRoomfalse"));
		alert.show();
	    }

	} else {
	    Alert alert = new Alert(AlertType.WARNING, t.getString("chatroom.alertEnterRoom2"));
	    alert.show();
	}
	stopReader = false;
    }

    // Raum verlassen
    public void leaveRoom() throws Exception {
	stopReader = true;
	String msgOut = Message.msgString(MsgArgs.LeaveChatroom, sl.getConfiguration().getToken(), mainRoom,
		sl.getConfiguration().getName());
	sl.getConfiguration().sendMsg(msgOut);
	String receive = sl.getConfiguration().receiveMsg();
	Message msgIn = new Message(receive);
	if (msgIn.isMsgOk()) {
	    mainRoom = "--";
	    chatter.clear();
	    actualMessages.clear();
	}
	startStop();
    }

    // NOCH NICHT FERTIG: Soll einkommende Chatnachrichten einlesen
//    class IncomeReader implements Runnable {	
//    	public void run() {     	    
//    	    String receive = null;	    
//    	    while(!(stopReader)) {	
//		try { 
//		   receive = sl.getConfiguration().receiveMsg();
//		   if(receive == "") {
//			System.out.println("Keine neue Nachricht");
//		   }else {
//			Message msgIn = new Message(receive);
//			if(msgIn.getMsgType().equals(MsgType.MessageText.toString())) {  
//			    msgIn.setRead(true);
//			 actualMessages.add(msgIn.getName()+": "+msgIn.getText());		
//			    messages.add(msgIn);
//			}
//		    Thread.sleep(400);
//		    System.out.println("read");
//		    Thread.sleep(400);
//		   }
//		}catch (Exception e) {
//		    // TODO Auto-generated catch block
//		    e.printStackTrace();
//		}  	
//    } 
//    }
//    	    }
    // Soll verhindern, dass beim absetzten von Nachrichten Stream falsch ankommt.
    public void startStop() {
	if (stopReader) {
	    stopReader = false;
//	    IncomeReader reader = new IncomeReader();
//	    Thread read = new Thread(reader);
//	    read.setDaemon(true);
//	    read.start();
	} else {
	    stopReader = true;
	}
    }

    public static String getMainRoom() {
	return mainRoom;
    }

    public static void addMessage(Message msgIn) {
	messages.add(msgIn);
    }

    public ObservableList<String> getActMsg() {
	return actualMessages;
    }

    public static ObservableList<String> getRooms() {
	return chatrooms;
    }

    public ObservableList<String> getChatter() {
	return chatter;
    }

}
