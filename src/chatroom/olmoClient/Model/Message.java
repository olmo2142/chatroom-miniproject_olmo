package chatroom.olmoClient.Model;

import java.util.ArrayList;

import chatroom.olmoClient.MsgArgs;
import chatroom.olmoClient.MsgType;
import chatroom.olmoClient.ServiceLocator;

public class Message implements Comparable<Message> {

    ServiceLocator sl = ServiceLocator.getServiceLocator();

    static int id = -1;

    private int msgID;
    private String type; // nur bei Message Text verwendet
    private String name;
    private String token;
    private String target; // chatroom or user
    private String text;
    private String direction;
    private boolean read;
    private boolean msgOk;
    private ArrayList<String> elements;

    public static String msgString(MsgArgs commands) {
	String message = commands.toString();
	return message.toString();
    }

    public static String msgString(MsgArgs commands, String a) {
	String message = commands.toString() + "|" + a;
	return message.toString();
    }

    public static String msgString(MsgArgs commands, String a, String b) {
	String message = commands.toString() + "|" + a + "|" + b;
	return message.toString();
    }

    public static String msgString(MsgArgs commands, String a, String b, String c) {
	String message = commands.toString() + "|" + a + "|" + b + "|" + c;
	return message.toString();
    }

    public Message(String receive) {
	id++;
	this.msgID = id;
	this.msgOk = false;
	this.read = false;
	String[] values = receive.split("\\|");
	if (values.length == 2 && values[0].equals(MsgType.Result.toString()) && values[1].equals("true")) {
	    this.type = values[0];
	    this.msgOk = true;

	}
	if (values.length == 2 && values[0].equals(MsgType.MessageError.toString())) {
	    this.type = values[0];
	    this.msgOk = false;
	}
	if (values.length == 3 && values[0].equals(MsgType.Result.toString()) && values[1].equals("true")
		&& sl.getConfiguration().isLoginSucces() == false) {
	    this.type = values[0];
	    this.msgID = id;
	    sl.getConfiguration().setLoginSucces(true);
	    sl.getConfiguration().setToken(values[2]);
	    this.msgOk = true;
	}

	if (values.length >= 3 && values[0].equals(MsgType.Result.toString()) && values[1].equals("true")
		&& sl.getConfiguration().isLoginSucces() == true) {
	    this.type = values[0];
	    elements = new ArrayList<String>();
	    for (int i = 2; i < values.length; i++) {
		elements.add(values[i]);
	    }
	    this.type = values[0];
	    this.msgOk = true;
	}

	if (values[0].equals(MsgType.MessageText.toString())) {
	    this.type = values[0]; // MessageText
	    this.name = values[1]; // Name of User
	    this.target = values[2]; // Traget (chatroom or user)
	    this.text = values[3]; // Message Text
	    if (this.name.equals(sl.getConfiguration().getName())) {
		this.direction = "out";
	    } else {
		this.direction = "in";
	    }
	}
	this.msgOk = true;
    }

    public String toString() {
	return (msgID + type + name + token + target + text + direction + "");
    }

    public boolean isMsgOk() {
	return msgOk;
    }

    public int getID() {
	return msgID;
    }

    public String getMsgType() {
	return type;
    }

    public String getTarget() {
	return target;
    }

    public String getName() {
	return name;
    }

    public String getText() {
	return text;
    }

    public boolean getRead() {
	return read;
    }

    public void setRead(boolean read) {
	this.read = read;
    }

    public ArrayList<String> getList() {
	return elements;
    }

    @Override
    public int compareTo(Message o) {
	return Integer.compare(this.id, o.getID());
    }

}
