package chatroom.olmoClient.Model;

import chatroom.olmoClient.ServiceLocator;
import chatroom.olmoClient.Translator;
	

public abstract class Basic_Model {
    protected ServiceLocator sl;
    protected Translator t;
    
    protected Basic_Model() {
	sl = ServiceLocator.getServiceLocator();
	t = ServiceLocator.getServiceLocator().getTranslator();
    }

}
